#!/bin/sh

if [ -z "$PYTHON" ] ; then PYTHON=python ; fi

cd $(dirname $0)
export PYTHONPATH=$(realpath ..):$PYTHONPATH

$PYTHON ./lowlevel2.py -q \
    -dNOPAUSE -dBATCH -dSAFER -sDEVICE=pdfwrite -sOutputFile=/tmp/out.pdf \
    -f ./test.ps
