#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of python-ghostscript.
# Copyright 2010-2023 by Hartmut Goebel <h.goebel@crazy-compilers.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

__author__ = "Hartmut Goebel <h.goebel@crazy-compilers.com>"
__copyright__ = "Copyright 2010-2023 by Hartmut Goebel <h.goebel@crazy-compilers.com>"
__licence__ = "GNU General Public License version 3 (GPL v3)"
__credits__ = "Based on an example from https://ghostscript.readthedocs.io/en/gs10.0.0/API.html"

import sys
from ghostscript import _gsprint as gs
import locale

command = b"1 2 add == flush\n"

# For the low-level interface arguments have to be bytes. Encode them
# using local encoding to save calling set_arg_encoding().
encoding = locale.getpreferredencoding()
args = sys.argv
args = [a.encode(encoding) for a in args]

instance = gs.new_instance()

code = 0
try:
    gs.init_with_args(instance, args)
    gs.run_string_begin(instance)
    gs.run_string_continue(instance, command)
    gs.run_string_continue(instance, b"qu")
    gs.run_string_continue(instance, b"it")
    gs.run_string_end(instance)
except gs.GhostscriptError as e:
    code = e.code
    if code != gs.e_Quit:
        print("error raised:", e)
finally:
    code1 = gs.exit(instance)
    if code in (0,  gs.e_Quit):
        code = code1

gs.delete_instance(instance)
if code not in (0,  gs.e_Quit):
    sys.exit(1)
